import boto3
import parser
import re
import unicodedata
import generate_feed
import sys


BUCKET = 'podaudiobook'


def strip_accents(text):

    try:
        text = unicode(text, 'utf-8')
    except NameError:  # unicode is a default on python 3
        pass

    text = unicodedata.normalize('NFD', text)\
           .encode('ascii', 'ignore')\
           .decode("utf-8")

    return str(text)


def get_valid_filename(s):
    s = str(s).strip().replace(' ', '_')
    p = strip_accents(re.sub(r'(?u)[^-\w.]', '', s))

    return p


def send_to_polly(bucket, prefix, data, lang):
    '''
    Text to speech , we save to S3
    Inputs : bucket = bucket name
            prefix = file prefix on s3
            data = data string to encode
    Returns : TaskID
    '''
    polly = boto3.client('polly', region_name='ap-southeast-2')

    if lang == "fr-FR":
        voiceid = 'Lea'
        engine='standard'

    if lang == "en-GB":
        voiceid = 'Amy'
        engine='neural'


    response = polly.start_speech_synthesis_task(VoiceId=voiceid,
                                                 OutputS3BucketName=bucket,
                                                 Engine=engine,
                                                 LanguageCode=lang,
                                                 OutputFormat='mp3',
                                                 SampleRate='24000',
                                                 OutputS3KeyPrefix=prefix,
                                                 TextType='text',
                                                 Text=data)

    taskId = response['SynthesisTask']['TaskId']
    return taskId


if __name__ == "__main__":

    book = sys.argv[1]
    data = parser.parse(book)

    foldername = get_valid_filename(f"{data['author']}_{data['title']}") + '/'
    prefix = get_valid_filename(f"{data['author']}-{data['title']}")

    file_list = []

    if "fr" in data['language']:
        lang = "fr-FR"
    else:
        lang = "en-GB"

    for i in data['chapters']:

        chapter_name = get_valid_filename(i[0])
        response_polly = send_to_polly(BUCKET,
                                       foldername + f"{prefix}-{chapter_name}",
                                       i[1], lang)
        file_list.append(
            [i[0], f"{prefix}-{chapter_name}" + f".{response_polly}.mp3"])




    local_name = get_valid_filename(f"{data['author']}-{data['title']}")
    cover = parser.get_cover(book, local_name)
    generate_feed.upload_cover_to_s3(BUCKET, local_name, foldername)
    generate_feed.generate_feed(data['title'], file_list, foldername, local_name)
    generate_feed.upload_feed_to_s3(BUCKET, local_name, foldername)
