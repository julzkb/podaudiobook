import io
import sys
import logging
from ebooklib import epub
from bs4 import BeautifulSoup
from PIL import Image

# logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.WARNING)


def get_chapters(b):
    """
    Inputs : b = epub book
    Returns: List of chapter objects
    """

    chapters = []
    for i in b.get_items():
        try:
            if i.is_chapter():
                if "titlepage" not in i.get_name():
                    chapters.append(i)
        except:
            pass

    return chapters


def is_real_chapter(b, c):
    """
    Inputs:
        b : epub book
        c : chapter object

    Returns:
        Bool
    """

    chapter = b.get_item_with_id(c.id)

    # print("")
    # print(chapter.get_body_content()[0:400])

    s = BeautifulSoup(chapter.get_body_content(), features="lxml")

    if len(s.findAll('div', {"class": "chapter"})) > 0:
        return True
    if len(s.findAll('body', {"epub:type": "bodymatter"})) > 0:
        return True
    if len(s.findAll('h1', {"class": "bmtit"})) > 0:
        return True

    if len(s.findAll('body', {"class": "calibre"})) > 0:
        if len(s.findAll('div', {"class": "image"})) > 0:
            return False
        if len(s.findAll('div', {"class": "tocentry"})) > 0:
            return False

        else:
            return True

    else:
        return False


def get_chapter_title(b, c):
    """
    Tries to grab the Chapter title.
    Inputs:
        b : epub
        c: chapter id
    """

    chapter = b.get_item_with_id(c.id)

    logging.info(str(chapter.get_body_content()[0:300]))
    logging.info("")
    s = BeautifulSoup(chapter.get_body_content(), features="lxml")

    title = None

    chapter_tags = [['h1'], ['div', {
        "class": "title-chapter"
    }], ['div', {
        "class": "title-toc"
    }], ['h2', {
        "class": "ch-title"
    }]]

    for z in chapter_tags:
        # print(len(z))
        # if len(z) > 1 and type(z[1]) is dict:
        #     if s.find(z[0], z[1]):
        #         # print("yay")
        #         # print(s.find(z)[0:50])
        #         title = s.find(z).get_text()
        # print(title)
        if s.find(z):
            title = s.find(z).get_text()

    if title:
        return title
    else:
        return "Untitled"


def get_chapter_text(b, c):
    """
    Returns chapter text
    Inputs :
        b = epub
        c = chapter id
    Returns : Chapter text
    """

    chapter = b.get_item_with_id(c.id)
    soup = BeautifulSoup(chapter.get_body_content(), features="lxml")

    return soup.get_text()


def get_cover(b, filename):
    """
    Grabs cover file and save it
    Inputs : 
        b : book file path
        filename : local cover file name
    Returns : saved image path
    """

    book = epub.read_epub(b)

    for i in book.get_items():
        logging.info(i)

    image = book.get_item_with_id('cover')

    if image:
        im = Image.open(io.BytesIO(image.get_content()))
    else:
        image = book.get_item_with_id('cover-image')
        im = Image.open(io.BytesIO(image.get_content()))

    im.save(filename + '.jpg', 'JPEG')

    if Image.open(filename + '.jpg'):
        return filename + '.jpg'
    else:
        return None


def get_metadata(b):
    """
    Grabs metadata from an epub
    Input : b = epub
    Returns : Dict with Author, title, language
    """

    author = b.get_metadata('DC', 'creator')[0][0]
    title = b.get_metadata('DC', 'title')[0][0]
    language = b.get_metadata('DC', 'language')[0][0]

    return {'author': author, 'title': title, 'language': language}


def parse(b):

    book = epub.read_epub(b)
    metadata = get_metadata(book)
    parsed_book = {}
    parsed_book['author'] = metadata['author']
    parsed_book['title'] = metadata['title']
    parsed_book['language'] = metadata['language']
    parsed_book['chapters'] = []

    chapters = get_chapters(book)

    for i in chapters:
        if is_real_chapter(book, i):
            parsed_book['chapters'].append(
                [get_chapter_title(book, i),
                 get_chapter_text(book, i)])

            # parsed_book['chapters'].append(
            #     [get_chapter_title(book, i),
            #      get_chapter_text(i, book)[0:200]])

    return parsed_book


if __name__ == "__main__":

    book = sys.argv[1]
    b = epub.read_epub(book)

    print(get_cover(book, "cover"))
    print(get_chapters(b))

    # for i in get_chapters(b):

    # for c in b.get_items():
    #     print(c)

    print("----------------------------")
    print("Debug Results")

    parsed = parse(book)
    for i in parsed.keys():

        if "chapters" in i:
            print("Chapters :")
            for j in parsed[i]:
                print(j)
                print("")

        else:
            print(f"{i} : {parsed[i]}")
