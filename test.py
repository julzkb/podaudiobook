import unittest
import parser
import glob
from ebooklib import epub

epub_files = list(glob.glob('epub/*.epub'))


class TestParserMeta(type):
    def __new__(mcs, name, bases, dict):
        def gen_test_get_chapters(b):
            def test_get_chapters(self):
                book = epub.read_epub(b)
                x = parser.get_chapters(book)
                self.assertGreater(len(x), 1)

            return test_get_chapters

        def gen_test_is_real_chapter(b):
            def test_is_real_chapter(self):
                book = epub.read_epub(b)
                for i in parser.get_chapters(book):
                    x = parser.is_real_chapter(book, i)

                    self.assertIsNotNone(x)

            return test_is_real_chapter

        def gen_test_get_chapter_title(b):
            def test_get_chapter_title(self):
                book = epub.read_epub(b)

                chapters = parser.get_chapters(book)
                untitled_chapters = []

                for i in chapters:
                    x = parser.get_chapter_title(book, i)
                    if x == "Untitled":
                        untitled_chapters.append(1)

                    self.assertIsNotNone(x)
                    self.assertLess(len(untitled_chapters),
                                       len(chapters) / 2)

            return test_get_chapter_title

        def gen_test_get_chapters_text(b):
            def test_get_chapters_text(self):
                book = epub.read_epub(b)
                for i in parser.get_chapters(book):
                    x = parser.get_chapter_text(book, i)

                    self.assertGreater(len(x), 1)

            return test_get_chapters_text

        def gen_test_get_cover(b):
            def test_get_cover(self):
                x = parser.get_cover(b, "cover")
                self.assertIsNotNone(x)

            return test_get_cover

        def gen_test_get_metadata(b):
            def test_get_metadata(self):
                book = epub.read_epub(b)
                x = parser.get_metadata(book)
                self.assertGreater(len(x['author']), 0)
                self.assertGreater(len(x['title']), 0)
                self.assertGreater(len(x['language']), 0)

            return test_get_metadata

        for b in epub_files:
            dict[f"test_get_chapters_{b}"] = gen_test_get_chapters(b)
            dict[f"test_is_real_chapter_{b}"] = gen_test_is_real_chapter(b)
            dict[f"test_get_chapter_title_{b}"] = gen_test_get_chapter_title(b)
            dict[f"test_get_chapters_text_{b}"] = gen_test_get_chapters_text(b)
            dict[f"test_get_cover_{b}"] = gen_test_get_cover(b)
            dict[f"test_get_metadata_{b}"] = gen_test_get_metadata(b)

        return type.__new__(mcs, name, bases, dict)


class TestParser(unittest.TestCase, metaclass=TestParserMeta):
    [...]


if __name__ == "__main__":
    unittest.main()
