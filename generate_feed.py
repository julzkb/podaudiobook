import boto3
import logging
from feedgen.feed import FeedGenerator
from botocore.exceptions import ClientError

BUCKET_URL = 'https://podaudiobook.s3-ap-southeast-2.amazonaws.com/'


def generate_feed(title, file_list, folder, podcast_filename):
    '''
    Generates and update the podcast feed
    Inputs : file list from S3
    Returns: None
    '''
    fg = FeedGenerator()
    fg.load_extension('podcast')
    fg.podcast.itunes_category('Books')
    fg.title(title)
    fg.description("ebook to podcast of : " + title)
    fg.logo(BUCKET_URL + folder + "cover.jpg")
    fg.link(href=BUCKET_URL + folder + 'podaudiocast.xml')

    file_list.reverse()
    for f in file_list:
        fe = fg.add_entry()
        fe.id(BUCKET_URL + folder + f[1])

        fe.title(f[0])
        fe.enclosure(BUCKET_URL + folder + f[1], 0, 'audio/mpeg')

    fg.rss_str(pretty=True)
    fg.rss_file(podcast_filename + '.xml')


def upload_feed_to_s3(bucket, filename, folder):
    '''
    Uploads the podcast feed to an S3 bucket with the right permissions
    Input : bucket
    Returns : s3 response
    '''
    s3_client = boto3.client('s3')

    try:
        response = s3_client.upload_file(filename + '.xml',
                                         bucket,
                                         folder + 'podaudiobook.xml',
                                         ExtraArgs={
                                             'ContentType': 'text/xml',
                                             'ACL': 'public-read'
                                         })
    except ClientError as e:
        logging.error(e)
        return False

    return True


def upload_cover_to_s3(bucket, filename, folder):
    '''
    Uploads the podcast feed to an S3 bucket with the right permissions
    Input : bucket
    Returns : s3 response
    '''
    s3_client = boto3.client('s3')

    try:
        response = s3_client.upload_file(filename + '.jpg',
                                         bucket,
                                         folder + "cover.jpg",
                                         ExtraArgs={
                                             'ContentType': 'image/jpg',
                                             'ACL': 'public-read'
                                         })
    except ClientError as e:
        logging.error(e)
        return False

    return True
